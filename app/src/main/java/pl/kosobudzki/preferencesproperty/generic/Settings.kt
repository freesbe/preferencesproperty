package pl.kosobudzki.preferencesproperty.generic

import android.content.SharedPreferences

/**
 * @author krzysztof.kosobudzki
 */
class Settings(sharedPreferences: SharedPreferences) {

    val appVersion: Int by sharedPreferences.int(APP_VERSION)
    val appVersionName: String? by sharedPreferences.string(APP_VERSION_NAME)
    val isUpToDate: Boolean by sharedPreferences.boolean(IS_UP_TO_DATE)

    private companion object Key {
        const val APP_VERSION = "APP_VERSION"
        const val APP_VERSION_NAME = "APP_VERSION_NAME"
        const val IS_UP_TO_DATE = "IS_UP_TO_DATE"
    }
}