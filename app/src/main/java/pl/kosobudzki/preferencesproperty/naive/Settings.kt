package pl.kosobudzki.preferencesproperty.naive

import android.content.SharedPreferences

/**
 * @author krzysztof.kosobudzki
 */
class Settings(sharedPreferences: SharedPreferences) {

    val appVersion: Int by IntPreference(sharedPreferences, APP_VERSION)
    val appVersionName: String? by StringPreference(sharedPreferences, APP_VERSION_NAME)
    val isUpToDate: Boolean by BooleanPreference(sharedPreferences, IS_UP_TO_DATE)

    private companion object Key {
        const val APP_VERSION = "APP_VERSION"
        const val APP_VERSION_NAME = "APP_VERSION_NAME"
        const val IS_UP_TO_DATE = "IS_UP_TO_DATE"
    }
}