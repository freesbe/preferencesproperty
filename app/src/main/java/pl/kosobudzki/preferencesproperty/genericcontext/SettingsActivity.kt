package pl.kosobudzki.preferencesproperty.genericcontext

import android.app.Activity

/**
 * @author krzysztof.kosobudzki
 */
class SettingsActivity : Activity() {

    private var appVersion: Int by intPreference(Key.APP_VERSION, 1)
    private var appVersionName: String? by stringPreference(Key.APP_VERSION_NAME, "first-version")
    private var isUpToDate: Boolean by booleanPreference(Key.IS_UP_TO_DATE)

    private companion object Key {
        const val APP_VERSION = "APP_VERSION"
        const val APP_VERSION_NAME = "APP_VERSION_NAME"
        const val IS_UP_TO_DATE = "IS_UP_TO_DATE"
    }
}