package pl.kosobudzki.preferencesproperty.naiveextension

import android.content.SharedPreferences
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * @author krzysztof.kosobudzki
 */
private class IntPreference(
        private val sharedPreferences: SharedPreferences,
        private val key: String,
        private val defaultValue: Int
) : ReadWriteProperty<Any, Int> {

    override fun getValue(thisRef: Any, property: KProperty<*>): Int =
            sharedPreferences.getInt(key, defaultValue)

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Int) =
            sharedPreferences.edit()
                    .putInt(key, value)
                    .apply()
}

private class BooleanPreference(
        private val sharedPreferences: SharedPreferences,
        private val key: String,
        private val defaultValue: Boolean
) : ReadWriteProperty<Any, Boolean> {

    override fun getValue(thisRef: Any, property: KProperty<*>): Boolean =
            sharedPreferences.getBoolean(key, defaultValue)

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Boolean) =
            sharedPreferences.edit()
                    .putBoolean(key, value)
                    .apply()
}

private class StringPreference(
        private val sharedPreferences: SharedPreferences,
        private val key: String,
        private val defaultValue: String?
) : ReadWriteProperty<Any, String?> {

    override fun getValue(thisRef: Any, property: KProperty<*>): String? =
            sharedPreferences.getString(key, defaultValue)

    override fun setValue(thisRef: Any, property: KProperty<*>, value: String?) =
            sharedPreferences.edit()
                    .putString(key, defaultValue)
                    .apply()
}

fun SharedPreferences.int(key: String, defaultValue: Int = 0): ReadWriteProperty<Any, Int> =
        IntPreference(this, key, defaultValue)

fun SharedPreferences.boolean(key: String, defaultValue: Boolean = false): ReadWriteProperty<Any, Boolean> =
        BooleanPreference(this, key, defaultValue)

fun SharedPreferences.string(key: String, defaultValue: String? = null): ReadWriteProperty<Any, String?> =
        StringPreference(this, key, defaultValue)